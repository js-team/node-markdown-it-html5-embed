'use strict'

const path    = require('path')
const nodeResolve = require('@rollup/plugin-node-resolve').nodeResolve
const common  = require('@rollup/plugin-commonjs')
const json    = require('@rollup/plugin-json')

const BUNDLE  = process.env.BUNDLE === 'true'

let fileDest  = 'markdown-it-html5-embed.js'
const plugins = [
  nodeResolve({
      modulePaths: ['debian/node_modules', '/usr/share/nodejs']
  }),
  common(),
  json()
]
module.exports = {
  input: 'lib/index.js',
  output: {
    file: path.resolve(__dirname, `../dist/${fileDest}`),
    format: 'umd',
    name: 'markdownitHTML5Embed'
  },
  plugins
}
